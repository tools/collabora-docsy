# Collabora Docsy Hugo Theme Overrides

Some non destructive tweaks to the [Google Docsy Theme](https://github.com/google/docsy)

## How to use

Follow the [Docsy installation guide](https://github.com/google/docsy#prerequisites). There's a useful [demo project](https://github.com/google/docsy-example) you can refer to.

Then add the Collabora Docsy repo as a theme override:

```
git submodule add git@gitlab.collabora.com:tools/collabora-docsy.git themes/collabora-docsy
```

In your site's `config.toml` specify resolution order for theme files:

```
theme = ["collabora-docsy", "docsy"]
```

It is recommended iy the GitLab Pages docs to set `baseURL = "/project_name"` in your config, but I *do not* recommend doing this if you would like to use the local development server.

### Running a development server

Something like this in `run_docker.sh` will do:

```
#!/bin/sh

REPO=$(dirname $(realpath $0))

docker run \
  -p 1313:1313/tcp \
  -w /docs \
  -v ${REPO}:/docs:z \
  klakegg/hugo:0.91.2-ext-ci \
  hugo server
```

### Using with Gitlab Pages

An example `.gitlab-ci.yml`

```
# a "batteries included" base image from the Hugo team, via GL dep proxy for speeeed
image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/klakegg/hugo:0.91.2-ext-ci

variables:
  GIT_SUBMODULE_STRATEGY: recursive

test:
  tags:
    - lightweight
  script:
  - hugo
  artifacts:
    paths:
    - public
  except:
  - main

pages:
  tags:
    - lightweight
  script:
  - BASEURL=${CI_PAGES_URL##http:} # don't force stuff on plain HTTP when publishing on HTTPS
  - hugo -b "${BASEURL:-/}"
  artifacts:
    paths:
    - public
  only:
  - main
```

## Working with (and against) the theme

This supplements the [Docsy content guide](https://www.docsy.dev/docs/adding-content/content/).

### Folder structure example

```
content/
  en/
    _index.md
    docs/
      section_1.md
      section_2.md
      something_else.md
      a_big_section_folder/
        _index.md           # _index.md required to set section title
                            # and ensure section appears in TOC
        some_section.md
      images/
        # NOTE NO _index.md file!
      another_section_folder/
        _index.md           # you need _index.md at every level
        with_a_subsection/
          _index.md         # and here
          content.md

```

In summary: be liberal with your `_index.md` files, though they can simply contain the `title: ` pre-amble.


### Layouts

While all content in `content/en/docs` will be correctly formatted, the default Docsy *homepage* layout is [somewhat over-the-top](https://example.docsy.dev/). To use a saner layout, simply specify the 'docs' template for your `content/en/_index.md` page:

```
---
title: Hello world
type: docs
---

Blah blah
```

### Gitlab integration

This theme has a slight modification in order to support 'Edit This Page' links that point to a Gitlab repo (Docsy normally only supports GitHub).

To use it, add the following to `config.toml`:

```
gitlab_repo = "https://gitlab.collabora.com/<group>/<docs repo>"
gitlab_branch = "<branch name>"
```

### Embedding images and other relative URL nonsense

It can be quite confusing to work out the correct path to embedded images such that they render in a local development server and on GitLab Pages (per project), mostly to do with the way per-project Pages uses a subdirectoy to host your rendered content.

A nice, tidy solution, and one which leads to a much saner layout, is to disable pretty-urls in config.toml:

```
uglyurls = true
```

...place your images neatly alongside your content:

```
content/
  en/
    docs/
      my_article.md
      images/
        my_image.png

```

...and use the Docsy `figure` shortcode:

```
{{< figure src="images/my_image.png" title="A lovely image" >}}
```

As a consequence, your generated site URLs will map 1:1 markdown -> HTML, e.g. `docs/article.md` -> `docs/article.html` (without `uglyurls` would by default map to `docs/article/`).
